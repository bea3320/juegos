import Home from './Home/Home';
import TicTacToe from './TicTacToe/TicTacToe';
import Sudoku from './Sudoku/Sudoku';
import Hangman from './Hangman/Hangman';


export {
    Home,
    TicTacToe,
    Sudoku,
    Hangman
}