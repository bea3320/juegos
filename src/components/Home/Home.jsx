import { Link } from "react-router-dom";
import "./Home.scss";

const Home = () => {
  return (
    <div className="home">
      <ul className="link">
        <li className="text__game">
        <h2>Tres en Raya</h2>
          <Link to="/tictactoe">
            <img
              className="imgGame"
              src="https://www.pequeocio.com/wp-content/uploads/2010/09/tres-en-raya-reglas.jpg"
              alt="imagen tres en raya"
            ></img>
          </Link>
        </li>
        <li className="text__game">
        <h2>El Ahorcado</h2>
          <Link to="/hangman">
            <img
              className="imgGame"
              src="https://image.shutterstock.com/image-illustration/illustration-hangman-game-260nw-386156509.jpg"
              alt="imagen hangman"
            ></img>
          </Link>
        </li>
        <li className="text__game">
        <h2>Sudoku</h2>
          <Link to="/sudoku">
            <img
              className="imgGame"
              src="https://www.microsiervos.com/images/magic-sudoku.png"
              alt="imagen sudoku"
            ></img>
          </Link>
        </li>
      </ul>
    </div>
  );
};

export default Home;
