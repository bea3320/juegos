import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Home, TicTacToe, Sudoku, Hangman } from "./components";
import "./App.scss";

const App = () => {
  return (
    <Router>
      <div className="app">
        <div className="text">
          HuB De JueGoS
        </div>
        <div className="text__secondary">
          <h1>¡¡Bienvenido a tu web de juegos creado por Upgraders!!</h1>
          <h2>Elige tu juego favorito...</h2>
        </div>
        <div>
          <Switch>
            <Route exact path="/tictactoe" component={TicTacToe} />
          </Switch>
          <Switch>
            <Route exact path="/sudoku" component={Sudoku} />
          </Switch>
          <Switch>
            <Route exact path="/hangman" component={Hangman} />
          </Switch>
        </div>
        <div>
          <Home />
        </div>
      </div>
    </Router>
  );
};

export default App;
